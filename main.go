package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/docker/docker/client"
)

func main() {

	fmt.Printf("Started, listening on %s:%s \n", os.Getenv("GMI_HOST"), os.Getenv("GMI_PORT"))
	http.HandleFunc("/env", checkEnv)
	log.Fatal(http.ListenAndServe(":"+os.Getenv("GMI_PORT"), nil))
}

// Checks if the requested environment variables exist and their values
func checkEnv(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		params := r.URL.Query()
		i := 0

		fmt.Printf("Request received\n")
		// Creating new client in order to talk with docker engine

		cli, err := client.NewEnvClient()
		if err != nil {
			fmt.Fprintf(w, "Container Not Found")
			return
		}

		// Inspecting the container and saving the environment variables in the string array inspect

		inspect, err := cli.ContainerInspect(context.Background(), params.Get("name"))
		if err != nil {
			fmt.Fprintf(w, "Container Not Found")
			return
		}

		// Checking if params == docker's environment variables

		for pKey := range params {
			for eKey := range inspect.Config.Env {
				splitValues := strings.Split(inspect.Config.Env[eKey], "=")
				if pKey == splitValues[0] && params.Get(pKey) == splitValues[1] {
					i++
				}
			}
		}

		if i == len(params)-1 {
			fmt.Fprintf(w, "OK")
		} else {
			fmt.Fprintf(w, "ENV VAR NOT FOUND")
		}
	} else {
		fmt.Fprintf(w, "NOT A GET!")
	}
}
