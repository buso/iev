*** Settings ***

Documentation   This is a test 
...             Robot Framework is amazing

Library         Collections
Library         RequestsLibrary
Library         String
Library         BuiltIn

*** Test Cases ***  

Get Requests
    Create Session  IEVTest    http://iev_container:6969
    &{params} =   Create Dictionary   name=jenkins_container   JENKINS_HOME=/var/jenkins_home  
    ${resp}=    Get Request     IEVTest   /env   params=${params}
    Log To Console  ${resp.status_code}
    Log To Console  ${resp.text}
    Should Be Equal As Strings    ${resp.text}    OK